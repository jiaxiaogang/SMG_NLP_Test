//
//  SMGLanguageHeader.h
//  SMG_Language
//
//  Created by 贾  on 2017/4/6.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "GetVoiceCloudApi.h"
#import "VoiceCloudModel.h"

//是否空字符串
#define STRISOK(a) (a  && ![a isKindOfClass:[NSNull class]] && [a isKindOfClass:[NSString class]] && ![a isEqualToString:@""])

//字符串防闪
#define STRTOOK(a) (a  && ![a isKindOfClass:[NSNull class]]) ? ([a isKindOfClass:[NSString class]] ? a : [NSString stringWithFormat:@"%@", a]) : @""

//数组防闪
#define ARRTOOK(a) (a  && [a isKindOfClass:[NSArray class]]) ?  a : [NSArray new]

//数组取子防闪
#define ARR_INDEX(a,i) (a && [a isKindOfClass:[NSArray class]] && a.count > i) ?  a[i] : nil
