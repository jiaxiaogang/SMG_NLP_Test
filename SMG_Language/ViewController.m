//
//  ViewController.m
//  SMG_Language
//
//  Created by 贾  on 2017/3/16.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "ViewController.h"
#import "GetVoiceCloudApi.h"
#import "AFNetworking.h"
#import "SMGLanguageHeader.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    GetVoiceCloudApi *api = [[GetVoiceCloudApi alloc] init];
    api.text = @"国务院总理李克强调研上海外高桥时提出，支持上海积极探索新机制。";
    [api startWithCompletionBlockWithSuccess:^(GetVoiceCloudApi *request, NSMutableArray *modelArr) {
        if (modelArr) {
            for (VoiceCloudModel *model in modelArr) {
                NSLog(@"");
            }
        }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
